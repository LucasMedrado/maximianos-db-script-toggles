DROP TABLE IF EXISTS tbToggles CASCADE;
CREATE TABLE tbToggles
(
    idToggle BIGSERIAL NOT NULL UNIQUE PRIMARY KEY,
    idTogglePai BIGSERIAL,
    Nome VARCHAR(50),
    idLogin VARCHAR(15000)

);
CREATE UNIQUE INDEX CONCURRENTLY nome_index_unique ON tbToggles(Nome);
ALTER TABLE tbToggles ADD CONSTRAINT nome_unique UNIQUE USING INDEX nome_index_unique;

DROP TABLE IF EXISTS tbTogglesPorAmbiente CASCADE;
CREATE TABLE tbTogglesPorAmbiente
(
    idToggle BIGINT,
    idEnvironment INT,
    Enabled BIT

);

--DATABASE TOGGLES PARA USER APPLICATIO;
GRANT CONNECT ON DATABASE toggles TO application;
GRANT CONNECT ON DATABASE toggles TO application;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON ALL TABLES IN SCHEMA public TO application;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO application;


GRANT ALL PRIVILEGES ON DATABASE toggles TO adm;

select concat('ALTER TABLE ', kcu.table_name, ' CLUSTER ON ', tco.constraint_name, ';'),
       kcu.table_schema,
       kcu.table_name,
       tco.constraint_name,
       kcu.ordinal_position as position,
       kcu.column_name as key_column
from information_schema.table_constraints tco
join information_schema.key_column_usage kcu
     on kcu.constraint_name = tco.constraint_name
     and kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
where tco.constraint_type = 'PRIMARY KEY'
order by kcu.table_schema,
         kcu.table_name,
         position;

INSERT INTO tbtoggles VALUES (DEFAULT, 1, 'new feature');

INSERT INTO tbtogglesporambiente VALUES (1, 1, '1');
INSERT INTO tbtogglesporambiente VALUES (1, 2, '0');
INSERT INTO tbtogglesporambiente VALUES (1, 3, '0');
